# dump the data part of each chunk to individual files in the current directory
# warning - will make a real mess

import sys
import os

fileName = sys.argv[1]
print (fileName)
with open(fileName, mode='rb') as file: 
    fc = bytearray (file.read())                    # read entire file into byte array
    filesize = os.path.getsize(fileName)            # get size in bytes from OS
    print ("File is %d 0x%X bytes (from OS)" %(filesize, filesize))

    fp=0x18         # move the file pointer 24 bytes, skipping over the entire header
    found_count=0

    print ("{:<7}{:<33}{:<27}{:<19}{:<24}".format ("Type", "Mystery 10", "effs", "staticmaybe", "length"))

    while (fp<filesize):
    # while (found_count < 10):
        chunkStart=fp

        chunkHeader = fc [fp:fp+36]                 # byte array, whole chunk header

        chunkName = chunkHeader[0:4].decode()[::-1]
        chunkNameHex = chunkHeader [0:4]
        chunkTen = chunkHeader[4:14]
        chunkF = chunkHeader[14:22]
        chunkStatix = chunkHeader[22:28]

        chunkSizeBytes = chunkHeader[28:36]                # pull the size (64 bit unsigned int)
        chunkSize=int.from_bytes(chunkSizeBytes, "little", signed=False) # convert to int


        # print (chunkHeader.hex(sep=' '))

        # print ("%s   %s    %s    %s  %s (%d bytes)" % (chunkName, chunkTen.hex(sep=' '), chunkF.hex(sep=' '), chunkStatix.hex(sep=' '), chunkSizeBytes.hex(sep=' '), chunkSize))
        # print (chunkHeader[0:4])

        
        fp+=36  # fp now points at the frst byte of the chunk's data
        chunkData = fc [fp:fp+chunkSize] # pull the chunk's data



        # we do nothing with the chunk data for now

        outFile = "%04d - %s - %08X.bin" % (found_count, chunkName, chunkStart)
        print (outFile)

        newFile = open (outFile, "wb")
        newFile.write (chunkData)
        newFile.close

        fp += chunkSize                 # skip over all the chunk's data, should be at 1st byte of next chunk descriptor now
        found_count +=1

    print ("%d chunks found" % (found_count))
