# list the environment objects present in the file

from pathlib import Path
from tabulate import tabulate
import sys
import os

fileName = sys.argv[1]


class Chunk:
    def __init__ (self, header, data):
        self.header = header
        self.data = data

        self.type = self.header[0:4].decode()[::-1]

        #        --?-- ---??????-- ---OID??---         type/subtype???       always the same--  ---size of chunk data--
        # MSeq   02 00 03 00 00 00 00 00 00 00    ff ff ff ff ff ff ff ff    02 00 00 00 01 00  2c 01 00 00 00 00 00 00 (300 bytes)

        self.m1 = int.from_bytes(self.header[4:6], "little", signed=False)      # 02 00 above, think this is 16 bit int
        self.m2 = int.from_bytes(self.header[6:10], "little", signed=False)     
        self.m3 = int.from_bytes(self.header[10:14], "little", signed=False)    # there is correlation here in environment objs
        self.m4 = int.from_bytes(self.header[14:18], "little", signed=False)    # sometimes a number, sometimes f
        self.m5 = int.from_bytes(self.header[18:22], "little", signed=False)    # mostly ff ff ff ff sometimes ff ff ff 7f

        chunkSizeBytes = self.header[28:36]                # pull the size (64 bit unsigned int)
        chunkSize=int.from_bytes(chunkSizeBytes, "little", signed=False) # convert to int
    def __str__ (self):
        return  ("its shit")

    def __repr__ (self):
        return  self.header[0:4].decode()[::-1]

def readFile (fileName, chunkDestination):
    # print (fileName)
    with open(fileName, mode='rb') as file: 
        fc = bytearray (file.read())                    # read entire file into byte array
        filesize = os.path.getsize(fileName)            # get size in bytes from OS
        # print ("File is %d 0x%X bytes (from OS)" %(filesize, filesize))

        fp=0x18         # move the file pointer 24 bytes, skipping over the entire header
        found_count=0

        while (fp<filesize):
        # while (found_count < 10):
            chunkStart=fp
            chunkHeader = fc [fp:fp+36]                 # byte array, whole chunk header
            chunkSizeBytes = chunkHeader[28:36]                # pull the size (64 bit unsigned int)
            chunkSize=int.from_bytes(chunkSizeBytes, "little", signed=False) # convert to int
            fp+=36  # fp now points at the frst byte of the chunk's data
            chunkData = fc [fp:fp+chunkSize] # pull the chunk's data
            fp += chunkSize                 # skip over all the chunk's data, should be at 1st byte of next chunk descriptor now
            nc = Chunk(chunkHeader, chunkData)
            chunkDestination.append (nc)
            found_count +=1

        # print ("%d chunks found" % (found_count))

def readLayers (chunk, layersDestination, dumpDetails=True):
    
    first16 = chunk.data[0:16]
    numItems = (first16[1] - (first16[1])/16)/2
    if (dumpDetails):
        print ("First 16 bytes: %s" % first16.hex (sep=' '))
        print ("%d %d ---- %d layers predicted: (byte2 - byte2 / 16) / 2" % (first16[0], first16[1], numItems+1))

    fp = 0x10   # start of first short name
    layerCount=0
    lines=[]
    
    while layerCount < numItems:
        shortNameBytes = chunk.data[fp+1:fp+16]
        shortName = shortNameBytes.decode()
        shortName = shortName.split ("\x00")[0]
        fp+=16
        longNameLength = int.from_bytes(chunk.data[fp:fp+2], "little", signed=False)
        fp+=2
        longNameBytes = chunk.data [fp:fp+longNameLength]
        longName = longNameBytes.decode()
        fp+=longNameLength
        if longNameLength %2 ==0:
            fp+=16
        else:
            fp+=17
        
        # print ("{:<32} {:<32}".format (shortName, longNameLength))
        lines.append([layerCount, shortName, longName, longNameLength])
        layersDestination.append(longName)
        # print (f'LAYA {layerCount}: {shortName:>32}{longName:>32}{longNameLength:>32}')
        layerCount+=1

    if (dumpDetails):
        print (tabulate(lines, headers=["index", "shortname", "longname", "longname_length"],tablefmt="pretty", stralign="left"))

def getObjType(b):
    switcher = {
    0x18: "ornament",
    0x13: "monitor",
    0x04: "channel split",
    0x06: "fader/button",
    0x07: "keyboard",
    0x0F: "tranfsormer",
    0x0D: "physical in",
    0x0E: "sequencer in",
    0x00: "midi inst",
    0x11: "channel strip",
    0x12: "multi inst",
    0x05: "mapped inst",
    0x09: "MIDI Click"
    }
    return switcher.get (b,f'unknown (0x{b:02X})')

chunks=[]
readFile (fileName,chunks)
print ("ProjectData: %s\t%d chunks" % (Path(fileName).name, len(chunks)))

layers=[]
layerChunkFound=False
for c in chunks:
    if (c.type=="Layr"):
        print ("Layr chunk found. getting layers ....")
        layerChunkFound=True
        readLayers(c, layers, False)
        print (f'.... found {len(layers)} environment layers')

if (not layerChunkFound):                      # the one piece of error checking in this entire project to date
    print ("can't find layer chunk")
    quit()

t=[]
for c in chunks:
    if (c.type=="Envi"):
        objID =  int.from_bytes (c.header[0x0A:0x0E], "little", signed=False)

        posX = int.from_bytes (c.data[0x52:0x54], "little", signed=False)
        posY = int.from_bytes (c.data[0x54:0x56], "little", signed=False)
        width = int.from_bytes (c.data[0x56:0x58], "little", signed=False)
        height = int.from_bytes (c.data[0x58:0x5A], "little", signed=False)

        layer = int.from_bytes (c.data[0x5A:0x5E], "little", signed=False)
        objType = getObjType (c.data[0x51])

        shortNameBytes = c.data[0x74:0x94]
        shortName = shortNameBytes.decode()
        shortName = shortName.split ("\x00")[0]

        longNameLength = int.from_bytes(c.data[0x9E:0xA0], "little", signed=False)
        longNameBytes = c.data[0xA0:0xA0+longNameLength]
        longName = longNameBytes.decode()

        oTypeBytes = c.data[0x70:0x72]


     

        t.append ([f'{objID:X}', longName, layers[layer], objType, f'{posX},{posY}',f'{width}x{height}', oTypeBytes.hex(sep=' '), c.data[0x9B]])

print (tabulate(t, headers=["id?? ","long name", "on layer", "type", "pos","size", "0x70-0x71", "test"],tablefmt="pretty", stralign="left"))
